//
//  IImageDownloaderService.swift
//  TinkoffChat
//
//  Created by Sergey Shalnov on 23/11/2018.
//  Copyright © 2018 Sergey Shalnov. All rights reserved.
//

import UIKit


protocol IImageDownloadService {
    
    func performRequest(completion: @escaping (Int) -> Void)
    func webformatURL(index: Int) -> String?
    
    func load(index: Int, completion: @escaping (UIImage?) -> Void)
    
}
