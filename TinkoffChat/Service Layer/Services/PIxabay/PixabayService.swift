//
//  PixabayService.swift
//  TinkoffChat
//
//  Created by Sergey Shalnov on 23/11/2018.
//  Copyright © 2018 Sergey Shalnov. All rights reserved.
//

import UIKit


class PixabayService: IImageDownloadService {
    
    private let requestSender: IRequestSender
    private let requestLoader: IRequestLoader
    private var images: [PixabayImage]?
    
    init(requestSender: IRequestSender, requestLoader: IRequestLoader) {
        self.requestSender = requestSender
        self.requestLoader = requestLoader
    }
    
    func performRequest(completion: @escaping (Int) -> Void) {
        let requestConfig = RequestFactory.Pixabay.images()
        
        requestSender.send(requestConfig: requestConfig) { images in
            self.images = images
            
            if let count = images?.count {
                completion(count)

            } else {
                completion(0)
            }
        }
    }
    
    func webformatURL(index: Int) -> String? {
        guard let imageURL = images?[index].webformatURL else {
            return nil
        }
        
        return imageURL
    }
    
    func load(index: Int, completion: @escaping (UIImage?) -> Void) {
        guard let imageURL = images?[index].webformatURL else {
            completion(nil)
            return
        }
        
        requestLoader.load(url: imageURL) { (data) in
            guard let data = data else {
                completion(nil)
                return
            }
            
            completion(UIImage(data: data))
        }
    }
    
    
}
